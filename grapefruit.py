import sys
import datetime
import argparse
from bitstring import BitArray

# Parsing of arguments
parser = argparse.ArgumentParser()

parser.add_argument('-o', '--output',
    dest='output',
    help="output file")

parser.add_argument('-k', '--key',
    dest='key',
    required=True,
    help="key to program")

parser.add_argument('-f', '--format',
    dest='file_format',
    help="format of file")

args = parser.parse_args()

output_file = args.output
key = args.key
file_format = args.file_format

# Checks selected format
if (file_format == 'mfd') or (file_format == None):
    file_format = 'mfd'
elif file_format == 'mct':
    file_format = 'mct'
else:
    print("ERR: Not a valid format, use either mfd or mct")
    sys.exit()

# Checks for the filename, generates one if none was specified
if (output_file == None) and (file_format == 'mfd'):
    timenow = datetime.datetime.now()
    output_file = key + '_' + timenow.strftime("%H:%M:%S_%d-%m-%Y") + '.mfd'
elif (output_file == None) and (file_format == 'mct'):
    timenow = datetime.datetime.now()
    output_file = key + '_' + timenow.strftime("%H:%M:%S_%d-%m-%Y")

# Checks length of key
if len(key) != 9:
    print("ERR: Key has to be 9 hexadecimal characters")
    sys.exit()

# Opening template and replacing placeholder key
bin_file = BitArray(filename="template.mfd")
bin_file.replace('0xDEDEDEDED', '0x' + key)

if file_format == 'mfd':
    # Saving it to a new file
    new_bin_file = open(output_file, 'wb')
    bin_file.tofile(new_bin_file)
    new_bin_file.close()
elif file_format == 'mct':
    mct_file = open(output_file, 'w')
    n = 128
    sectors = [bin_file.hex[i:i+n] for i in range(0, len(bin_file.hex), n)]
    count = 0
    for sector in sectors:
        block_length = 32
        blocks = [sector[i:i+block_length] for i in range(0, len(sector), block_length)]
        mct_file.write('+Sector: ' + str(count) + '\n')
        for block in blocks:
            mct_file.write(block.upper() + '\n')
        count += 1
    mct_file.close()
